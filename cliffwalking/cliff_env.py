import math
import gym
from gym import spaces, logger
# from gym.utils import seeding
import numpy as np



class Coord:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def is_in_box(self, b_rows, b_cols):
        if self.x < 0 or self.y < 0:
            return False
        if self.x >= b_cols or self.y >= b_rows:
            return False
        return True

    def __add__(self, other):
        res = Coord(self.x + other.x, self.y + other.y)
        return res

    def __sub__(self, other):
        res = Coord(self.x - other.x, self.y - other.y)
        return res

    def __eq__(self, other):
        is_eq = self.x == other.x and self.y == other.y
        return is_eq

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        str_ = '2D Coordinate\n\tx: {}\n\ty: {}'.format(self.x, self.y)
        return str_


    def to_array(self):
        return np.array([self.x, self.y])




class CliffWalkEnv(gym.Env):

    metadata = {} # used for render modes


    def __init__(self):
        self.rows = 4
        self.cols = 12

        self.goal_coord = Coord(self.cols-1, 0)
        self.start_coord = Coord(0, 0)

        self.actions = [0, 1, 2, 3]
        self.action_space = spaces.Discrete(4)
        self.action_coord_dict = {0: Coord(-1, 0), 1: Coord(1, 0), 
                                  2: Coord(0, -1), 3: Coord(0, 1)}
        
        self.observation_space = spaces.Box(low=np.array([0, 0]),
                high=np.array([self.cols, self.rows]), shape=(2,), dtype=np.int)

        self.state = None
        self.episode_ended = None


    def step(self, action):
        err_msg = "%r (%s) invalid" % (action, type(action))
        assert self.action_space.contains(action), err_msg

        if self.episode_ended:
            print('[warning]: environment ended and has not been resetted')
            return self.state.to_array(), 0, True, {}

        next_state = self.state + self.action_coord_dict[action]
        
        if next_state == self.goal_coord:
            # goal
            self.state = next_state
            reward = -1
            done = True
            self.episode_ended = True

        elif next_state == self.start_coord or \
                next_state == self.start_coord + Coord(-1, 0):
            # start or one left to start
            self.state = self.start_coord
            reward = -1
            done = False

        elif next_state.y == 0:
            # note: goal and start are already checked
            # fall off cliff
            self.state = self.start_coord
            reward = -100
            done = False

        else:
            if next_state.is_in_box(self.rows, self.cols):
                self.state = next_state
            reward = -1
            done = False

        return self.state.to_array(), reward, done, {}

        
    def reset(self):
        self.state = self.start_coord
        self.episode_ended = False
        return self.state.to_array()



