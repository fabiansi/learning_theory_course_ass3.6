import numpy as np
import math

from abc import ABC, abstractmethod
from random import choice

from cliff_env import CliffWalkEnv




class StateCoords:
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.idx_list = self._get_indexlist()
        self.no_states = len(self.idx_list)

    def _get_indexlist(self):
        idx_list = list()
        for y in range(self.rows):
            for x in range(self.cols):
                idx_list.append([x, y])
        return idx_list

    def coord_from_idx(self, idx):
        return np.array(self.idx_list[idx])

    def idx_from_coord(self, coord):
        return self.idx_list.index(list(coord))



class RLAlgorithm(ABC):

    def __init__(self):
        self.alpha = .5
        self.epsilon = .1
        self.gamma = 1. # TODO not known, maybe better choice possible.

        self.state_coords = StateCoords(rows=4, cols=12)
        self.env = CliffWalkEnv()
        self.q = self._init_q()
        self.max_episodes = 500
        self.acc_rewards = None

    def _init_q(self):
        no_states = self.state_coords.no_states
        no_actions = len(self.env.actions)
        # q = np.zeros((no_states, no_actions)) 
        q = np.random.randn(no_states, no_actions)

        # important: terminal state = 0
        term_state = self.env.goal_coord.to_array()
        term_idx = self.state_coords.idx_from_coord(term_state)
        q[term_idx] = 0
        return q

    def _get_epsilon_greedy_action(self, state):
        if np.random.rand() > self.epsilon:
            # pick greedy action
            state_actions = self.q[self.state_coords.idx_from_coord(state)]
            a = np.argmax(state_actions)
        else:
            # sample random action
            a = choice(self.env.actions)
        return a

    @abstractmethod
    def _loop_steps(self, state):
        pass

    def loop_episodes(self):
        self.acc_rewards = list()

        for ep_idx in range(self.max_episodes):
            print('[I] running episode nr. {}'.format(ep_idx))
            
            state = self.env.reset()

            self._loop_steps(state)
            

class SARSA(RLAlgorithm):
    def _get_policy_update(self, s, a, r, s_n, a_n):
        s_idx = self.state_coords.idx_from_coord(s)
        s_n_idx = self.state_coords.idx_from_coord(s_n)
        
        next_q = self.q[s_idx, a] + self.alpha * \
                (r + self.gamma * self.q[s_n_idx, a_n] - self.q[s_idx, a])
        
        return next_q

    def _loop_steps(self, state):
        done = False
        step_count = 0
        reward_sum = 0

        # first action
        action = self._get_epsilon_greedy_action(state)

        while not done:
            # take action, observe next state, reward
            next_state, reward, done, _ = self.env.step(action)

            # pick new action
            next_action = self._get_epsilon_greedy_action(next_state)
            
            # policy update
            s_idx = self.state_coords.idx_from_coord(state)
            self.q[s_idx, action] = self._get_policy_update(state, action,
                    reward, next_state, next_action)

            # update state and action
            state = next_state
            action = next_action
            step_count += 1
            reward_sum += reward

        print('[I] episode finished after {} steps, accumulated rewards {}'.format(
            step_count, reward_sum))
        self.acc_rewards.append(reward_sum)


class QLearning(RLAlgorithm):
    def _get_policy_update(self, s, a, r, s_n):
        s_idx = self.state_coords.idx_from_coord(s)
        s_n_idx = self.state_coords.idx_from_coord(s_n)
        max_a_q = np.amax(self.q[s_n_idx])
        
        next_q = self.q[s_idx, a] + self.alpha * \
                (r + self.gamma * max_a_q - self.q[s_idx, a])
        
        return next_q


    def _loop_steps(self, state):
        done = False
        step_count = 0
        reward_sum = 0

        while not done:
            action = self._get_epsilon_greedy_action(state)

            # take action, observe next state, reward
            next_state, reward, done, _ = self.env.step(action)
            
            # policy update
            s_idx = self.state_coords.idx_from_coord(state)
            self.q[s_idx, action] = self._get_policy_update(state, action,
                    reward, next_state)

            # update state and action
            state = next_state
            step_count += 1
            reward_sum += reward

        print('[I] episode finished after {} steps, accumulated rewards {}'.format(
            step_count, reward_sum))
        self.acc_rewards.append(reward_sum)


class ExpectedSARSA(QLearning):
    def _get_policy_update(self, s, a, r, s_n):
        s_idx = self.state_coords.idx_from_coord(s)
        s_n_idx = self.state_coords.idx_from_coord(s_n)
        
        # calc probabilities that a is picked under current policy (\epsilon
        # greedy)
        state_actions = self.q[s_n_idx]
        max_actions = (state_actions == np.amax(state_actions)).astype(np.int)
        # normalize in case there are multible equivalent actions
        max_actions = max_actions / np.sum(max_actions)
        # formula to compute the policies propability of picking an action
        # p =   epsilon/no_actions              for all actions besides greedy
        #       epsilon/no_actions + (1-self.epsilon)       for greedy
        p_action = np.array([self.epsilon/len(max_actions) + fac * (1-self.epsilon)
                for fac in max_actions])
        
        exp_a_q = np.sum(p_action * state_actions)
        
        next_q = self.q[s_idx, a] + self.alpha * (r + self.gamma * exp_a_q -
                self.q[s_idx, a])
        
        return next_q


if __name__ == '__main__':

    sarsa_r = list()
    qlearn_r = list()
    expsarsa_r = list()

    for _ in range(1):
        sarsa = ExpectedSARSA()
        sarsa.loop_episodes()
        sarsa_r.append(sarsa.acc_rewards)

        qlearn = QLearning()
        qlearn.loop_episodes()
        qlearn_r.append(qlearn.acc_rewards)

        expsarsa = ExpectedSARSA()
        expsarsa.loop_episodes()
        expsarsa_r.append(expsarsa.acc_rewards)

    sarsa_r = np.array(sarsa_r)
    sarsa_avg = np.mean(sarsa_r, axis=0)

    qlearn_r = np.array(qlearn_r)
    qlearn_avg = np.mean(qlearn_r, axis=0)

    expsarsa_r = np.array(expsarsa_r)
    expsarsa_avg = np.mean(expsarsa_r, axis=0)

    import matplotlib.pyplot as plt
    plt.plot(sarsa_avg, label='sarsa')
    plt.plot(qlearn_avg, label='q-learning')
    plt.plot(expsarsa_avg, label='expected-sarsa')
    plt.legend()

    plt.show()

